const { Gtk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const SettingsType = ExtensionUtils.getCurrentExtension().imports.preferences.types.settingsType.SettingsType;

/* exported StringType */
var StringType = class extends SettingsType {
    get_widget() {
        return 'entry';
    }

    get_signal() {
        return 'activate';
    }

    get_value_from_widget(object) {
        return object.get_text();
    }

    update_widget(widget, setting_value) {
        widget.set_text(setting_value);
    }

    extra(params) {
        const focusController = new Gtk.EventControllerFocus();
        focusController.connect('leave', (ignored_object) => {
            params.settings.set(params.setting, this.get_value_from_widget(params.widget));
            this.update_widget(params.widget, this.get_value_from_widget(params.widget));
        });
        params.widget.add_controller(focusController);
    }
};

