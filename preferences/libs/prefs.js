const { Gtk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const Settings = Extension.imports.libs.settings.Settings;
const Shared = Extension.imports.libs.sharedConstants;
const Utils = Extension.imports.libs.utils;

const Preferences = Extension.imports.preferences;

/* exported Prefs */
var Prefs = class {

    constructor(schema, prefsWindow) {
        this.logger = Utils.getDefaultLogger();
        this.logger.level = Shared.LOG_INFO;
        const params = {
            settingsFile: `${Extension.path}/settings.json`,
            schema,
            logger: this.logger
        };

        this.settings = new Settings(params);
        this.builder = Gtk.Builder.new();
        this.builder.add_from_file(`${Extension.path}/preferences/ui/prefs.xml`);
        this.window = prefsWindow;
    }

    _optionToInteger(option) {
        const result = /^#(.*)#$/.exec(option);
        if (result !== null) {
            // eslint-disable-next-line no-magic-numbers
            const importPath = result[1].charAt(0).toLowerCase() + result[1].substring(1);
            const parts = importPath.split('.');
            const FIRST_PART = 0;
            let temp = Extension.imports[`libs/${parts[FIRST_PART]}`];
            for (let i = 1; i < parts.length; i++) {
                temp = temp[parts[i]];
            }
            return parseInt(temp);
        }
        return parseInt(option);
    }

    _convertOptionArrayToIndexedArray(optionArray) {
        const indexedOptions = {};
        for (const option in optionArray) {
            if (Object.prototype.hasOwnProperty.call(optionArray, option)) {
                indexedOptions[this._optionToInteger(option)] = optionArray[option];
            }
        }
        return indexedOptions;
    }

    _getSensitivityArrayFromData(data) {
        if (!Utils.isChildValid(data, 'extra_params')) {
            return null;
        }
        if (!Utils.isChildValid(data['extra_params'], 'sensitivity')) {
            return null;
        }
        return data['extra_params']['sensitivity'];
    }

    _isSensitive(data) {
        const sensitivityArray = this._getSensitivityArrayFromData(data);
        if (sensitivityArray === null) {
            return true;
        }
        for (const checkSetting in sensitivityArray) {
            if (Object.prototype.hasOwnProperty.call(sensitivityArray, checkSetting)) {
                let checkSettingArray = sensitivityArray[checkSetting];
                if (this.settings.getType(checkSetting) == 'integer') {
                    checkSettingArray = this._convertOptionArrayToIndexedArray(sensitivityArray[
                        checkSetting]);
                }
                const currentCheckSettingValue = this.settings.get(checkSetting);
                if (typeof checkSettingArray[currentCheckSettingValue] != 'undefined') {
                    return checkSettingArray[currentCheckSettingValue];
                }
            }
        }
        return true;
    }

    _registerSensitivity(data, callback) {
        const sensitivityArray = this._getSensitivityArrayFromData(data);
        if (sensitivityArray === null) {
            return;
        }
        for (const sensitivitySetting in sensitivityArray) {
            if (Object.prototype.hasOwnProperty.call(sensitivityArray, sensitivitySetting)) {
                this.settings.registerForChange(sensitivitySetting, callback);
            }
        }
    }

    _connectSettings(window) {
        for (const [setting, data] of this.settings.getAll()) {
            const type = Preferences.types.settingsType.getType(data);
            if (type === null) {
                this.logger.error(`No type for ${setting}`);
                continue;
            }
            const [valid, error] = type.validate(setting, data);
            if (!valid) {
                this.logger.error(error);
                continue;
            }
            const widget = this.builder.get_object(`${setting}-${type.get_widget()}`);
            if (widget === null) {
                this.logger.error(`No widget for ${setting}`);
                continue;
            }
            for (const template of type.additional_templates()) {
                this.builder.add_from_file(template);
            }
            if (type.get_signal() !== null) {
                widget.connect(type.get_signal(), (object) => {
                    this.settings.set(setting, type.get_value_from_widget(object));
                });
            }
            const params = {
                widget,
                setting,
                settings: this.settings,
                setting_data: data,
                builder: this.builder,
                prefsWindow: window,
                logger: this.logger
            };
            type.extra(params);
            type.update_widget(widget, this.settings.get(setting));
            this._registerSensitivity(data, () => {
                this.builder.get_object(`${setting}-${type.get_row()}`).sensitive = this._isSensitive(data);
            });
            this.builder.get_object(`${setting}-${type.get_row()}`).sensitive = this._isSensitive(data);
        }
    }

    fillPreferencesWindow(window) {
        const pages = ['general', 'display', 'priorities', 'extensions'];
        for (const page of pages) {
            window.add(this.builder.get_object(`${page}-page`));
        }
        this._connectSettings(window);
    }
};
